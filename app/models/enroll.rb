class Enroll < ActiveRecord::Base
  belongs_to :course, primary_key: "course_id"
  belongs_to :student, primary_key: "student_id"
end
