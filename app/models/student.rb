class Student < ActiveRecord::Base
  self.primary_key = "student_id"
  has_many :enrolls, dependent: :delete_all
  has_many :courses, :through => :enrolls
end
