class Course < ActiveRecord::Base
  self.primary_key = "course_id"
  has_many :enrolls, dependent: :delete_all
  has_many :students, :through => :enrolls
end
