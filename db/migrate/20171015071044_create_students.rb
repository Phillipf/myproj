class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students, :primary_key => :student_id do |t|
      t.integer :student_id
      t.string :name
      t.text :email

      t.timestamps null: false
    end
    #add_index :students, :student_id, unique: true
  end
end
