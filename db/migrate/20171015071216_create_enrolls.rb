class CreateEnrolls < ActiveRecord::Migration
  def change
    create_table :enrolls do |t|
      t.integer :student_id, index: true
      t.integer :course_id, index: true
      t.float :percentage
      t.string :lettergrade

      t.timestamps null: false
    end
    #add_index :enrolls, ["student_id", "course_id"]

  end
end
