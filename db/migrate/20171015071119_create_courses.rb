class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses, :primary_key => :course_id do |t|
      t.integer :course_id
      t.text :description

      t.timestamps null: false
    end
    #add_index :courses, :course_id, unique: true
  end
end
